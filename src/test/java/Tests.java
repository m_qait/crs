import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import hello.Application;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.testng.annotations.*;

public class Tests {
    ConfigurableApplicationContext ctx;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

        ctx = SpringApplication.run(Application.class);
    }

    @Test
    public void test_01_validate_that_server_is_started() throws UnirestException, JSONException {
      HttpResponse<JsonNode> resp =  Unirest.post("http://localhost:8080/compress").body("{\n" +
               "    \"inputDir\": \"/Users/mandeepsingh/Desktop/inp\",\n" +
               "    \"outputDir\": \"/Users/mandeepsingh/Desktop/out\",\n" +
               "    \"maxSize\": 4000000,\n" +
               "    \"speed\": 0\n" +
               "}").asJson();
      //TODO: Adding more assertions

    }
    @AfterClass
    public void tearDown() throws Exception {
        ctx.close();

    }
}
