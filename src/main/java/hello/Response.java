package hello;

public class Response {

    private final long id;
    private final String status;

    public Response(long id, String status) {
        this.id = id;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }
}
