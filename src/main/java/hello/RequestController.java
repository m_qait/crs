package hello;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.web.bind.annotation.*;


@RestController
public class RequestController {

    private static final String template = "%s";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/compress", method = RequestMethod.POST)
    public Response compress(@RequestBody() String body) throws IOException {
        JSONObject json = new JSONObject(body);
        String inputDir;
        String outputDir;
        InputStream st = RequestController.class.getResourceAsStream("/inputSchema.json");
        if(this.requestValidator(json.toString(),st)){
            inputDir = json.getString("inputDir");
            outputDir = json.getString("outputDir");
            if(json.getString("inputDir").equals("default")){
                inputDir = RequestController.class.getResource("/inputDir").getPath();
            }
            if(json.getString("outputDir").equals("default")){
                outputDir = RequestController.class.getResource("/outputDir").getPath();
            }
            else{
                File f = new File(inputDir);
                File f2 = new File(outputDir);
                if(!f.exists()){
                    return new Response(counter.incrementAndGet(), "Provided input directory does not exists....");
                }
                if(!f2.exists()){
                    return new Response(counter.incrementAndGet(), "Provided output directory does not exists....");
                }
            }
            this.compressor(inputDir,outputDir,4,0);
            return new Response(counter.incrementAndGet(), "done");
        }

        else{ return new Response(counter.incrementAndGet(), "Invalid body... Please check documentation");}

    }

    @RequestMapping(value = "/decompress", method = RequestMethod.POST)
    public Response decompress(@RequestBody() String body) throws IOException {
        JSONObject json = new JSONObject(body);
        String inputDir;
        String outputDir;


        if(requestValidator(json.toString(),RequestController.class.getResourceAsStream("/outputSchema.json"))){
            inputDir = json.getString("inputDir");
            outputDir = json.getString("outputDir");
            if(json.getString("inputDir").equals("default")){
                inputDir = RequestController.class.getResource("/outputDir").getPath();
            }
            if(json.getString("outputDir").equals("default")){
                outputDir = RequestController.class.getResource("/inputDir").getPath();
            }
            else{
                File f = new File(inputDir);
                File f2 = new File(outputDir);
                if(!f.exists()){
                    return new Response(counter.incrementAndGet(), "Provided input directory does not exists....");
                }
                if(!f2.exists()){
                    return new Response(counter.incrementAndGet(), "Provided output directory does not exists....");
                }
            }
            this.deCompressor(inputDir,outputDir);
            return new Response(counter.incrementAndGet(), "done");
        }

        else{ return new Response(counter.incrementAndGet(), "Invalid body... Please check documentation");}

    }

    private boolean requestValidator(String request, InputStream inschema){
        try {
            Schema schema = SchemaLoader.load(new JSONObject(new JSONTokener(inschema)));
            schema.validate(new JSONObject(request));
            return true;
        }catch (ValidationException  e){return false;}
    }

    private void compressor(String inputDir, String outputDir, int maxSize, int speed) throws IOException {
        // TODO: Improve size handling
        byte[] buffer = new byte[4194304];
        Map<File,String> files = listFiles(inputDir);
        Set<File> fl = files.keySet();
        int size = fl.size();
        int fileNum=1;
        for(File f : fl) {
            FileOutputStream fos = new FileOutputStream(outputDir+File.separator+"File"+fileNum+".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            ZipEntry ze = new ZipEntry(f.getAbsolutePath().replace(inputDir,""));
            // internal directory structure as name
            zos.putNextEntry(ze);
            FileInputStream in = new FileInputStream(f.getAbsolutePath());
            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            in.close();
            zos.closeEntry();
            zos.close();
            fileNum++;
        }
    }

    private void deCompressor(String inputDir, String outputDir) throws IOException {
        byte[] buffer = new byte[4194304];
        //create output directory is not exists
        File folder = new File(outputDir);
        if(!folder.exists()){
            folder.mkdir();
        }
        File inDir = new File(inputDir);
        //get the zip file content
        File[] fList = inDir.listFiles();
        for(File f:fList){
            ZipInputStream zis =
                    new ZipInputStream(new FileInputStream(f));
            ZipEntry ze = zis.getNextEntry();
            while(ze!=null){
                String fileName = ze.getName();
                File newFile = new File(outputDir + File.separator + fileName);
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        }
    }

    public  Map<File,String> listFiles(String directoryName){
        Map<File,String> files = new HashMap<>();
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isFile()){
                files.put(file,file.getName());
            } else if (file.isDirectory()){
                listFiles(file.getAbsolutePath());
            }
        }
        return files;
    }
}